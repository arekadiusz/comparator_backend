package de.psi.pjf.apiComparator;

import java.io.IOException;
import java.util.Scanner;

import de.psi.pjf.apiComparator.csv.ApiToCsvWriter;
import de.psi.pjf.apiComparator.csv.DiffToCsvWriter;
import de.psi.pjf.apiComparator.json.DiffToJsonWriter;

public class Application
{
    public static void main( String[] args ) throws IOException
    {
        final Scanner sc = new Scanner( System.in );

        System.out.println( "Choose option:" );
        System.out.println( "1. Generate Api" );
        System.out.println( "2. Generate diff .csv file" );
        System.out.println( "3. Generate diff .json file" );

        switch( sc.nextLine() )
        {
            case "1":
                ApiToCsvWriter apiToCsvWriter = new ApiToCsvWriter();
                System.out.println( "Input path to source folder:" );
                String pathToJars = sc.nextLine();
                apiToCsvWriter.write( pathToJars );
                break;
            case "2":
                DiffToCsvWriter diffToCsvWriter = new DiffToCsvWriter();
                System.out.println( "Input path to api of the old component file:" );
                String firstApiPath = sc.nextLine();
                System.out.println( "Input path to api of the new component file:" );
                String secondApiPath = sc.nextLine();
                diffToCsvWriter.write( firstApiPath, secondApiPath );
                break;
            case "3":
                DiffToJsonWriter diffToJsonWriter = new DiffToJsonWriter();
                System.out.println( "Input path to api of the old component file:" );
                String firstPath = sc.nextLine();
                System.out.println( "Input path to api of the new component file:" );
                String secondPath = sc.nextLine();
                diffToJsonWriter.write( firstPath, secondPath );
                break;

            default:
                System.err.println( "You have chosen an incorrect option!" );
                break;
        }
    }
}
