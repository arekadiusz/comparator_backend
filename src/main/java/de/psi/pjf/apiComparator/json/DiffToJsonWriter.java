package de.psi.pjf.apiComparator.json;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.psi.pjf.apiComparator.comparator.ApiComparator;
import de.psi.pjf.apiComparator.comparator.DifferenceInApi;
import de.psi.pjf.apiComparator.csv.CsvFileReader;
import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

public class DiffToJsonWriter
{
    public void write( String firstApiPath, String secondApiPath ) throws IOException
    {
        File oldApiFile = new File( firstApiPath );
        CsvFileReader csvoldApiFileReader = new CsvFileReader();
        PjfComponentApi oldApi = csvoldApiFileReader.loadLinesFromFile( oldApiFile );

        File newApiFile = new File( secondApiPath );
        CsvFileReader csvnewApiFileReader = new CsvFileReader();
        PjfComponentApi newApi = csvnewApiFileReader.loadLinesFromFile( newApiFile );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );
        DifferenceInApi< Method > differenceInApiMethod = apiComparator.getMethodDiff();
        DifferenceInApi< Field > differenceInApiField = apiComparator.getFieldDiff();
        DifferenceInApi< Constructor > differenceInApiConstructor = apiComparator.getConstructorDiff();

        PackagesAndClassesParser packagesAndClassesParser = new PackagesAndClassesParser();

        packagesAndClassesParser.packagesAndClassesParse( differenceInApiMethod.getAddedMethods().stream(),
            (( aClass, aObject ) -> {
                aClass.addAddedMethod( (Method)aObject );
            }) );

        packagesAndClassesParser.packagesAndClassesParse( differenceInApiMethod.getRemovedMethods().stream(),
            (( aClass, aObject ) -> {
                aClass.addRemovedMethod( (Method)aObject );
            }) );

        packagesAndClassesParser.packagesAndClassesParse( differenceInApiField.getAddedFields().stream(),
            (( aClass, aObject ) -> {
                aClass.addAddedField( (Field)aObject );
            }) );
        packagesAndClassesParser.packagesAndClassesParse( differenceInApiField.getRemovedFields().stream(),
            (( aClass, aObject ) -> {
                aClass.addRemovedField( (Field)aObject );
            }) );
        packagesAndClassesParser.packagesAndClassesParse(
            differenceInApiConstructor.getAddedConstructors().stream(), (( aClass, aObject ) -> {
                aClass.addAddedConstructor( (Constructor)aObject );
            }) );
        packagesAndClassesParser.packagesAndClassesParse(
            differenceInApiConstructor.getRemovedConstructors().stream(), (( aClass, aObject ) -> {
                aClass.addRemovedConstructor( (Constructor)aObject );
            }) );

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String json = gson.toJson( packagesAndClassesParser.getPackagesList() );

        FileWriter writer = new FileWriter( new File( firstApiPath ).getParent() + "\\diffAPI.json" );
        writer.write( json );
        writer.flush();
        writer.close();
    }
}
