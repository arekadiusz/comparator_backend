package de.psi.pjf.apiComparator.json;

import de.psi.pjf.apiComparator.processingdata.AbstractJavaApiElement;
import de.psi.pjf.apiComparator.processingdata.JavaClass;

public interface JsonInterpreterIf
{
    public void putJavaApiElementToCorrectList( JavaClass aClass, AbstractJavaApiElement aObject );
}
