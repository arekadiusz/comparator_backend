package de.psi.pjf.apiComparator.json;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import de.psi.pjf.apiComparator.processingdata.AbstractJavaApiElement;
import de.psi.pjf.apiComparator.processingdata.JavaClass;
import de.psi.pjf.apiComparator.processingdata.JavaPackage;

public class PackagesAndClassesParser
{
    private Set< JavaPackage > packagesList = new HashSet<>();

    public Set< JavaPackage > getPackagesList()
    {
        return packagesList;
    }

    public void packagesAndClassesParse( Stream< ? extends AbstractJavaApiElement > aApiElementStream,
        JsonInterpreterIf jsonInterpreter )
    {
        aApiElementStream.forEach( currentApiElement -> {
            String stringPackage = getJavaPackage( currentApiElement.getClassName() );
            String stringClass = getJavaClass( currentApiElement.getClassName() );

            Optional< JavaPackage > optionalExistingJavaPackage =
                packagesList.stream().filter( p -> p.getName().equals( stringPackage ) ).findAny();
            optionalExistingJavaPackage.ifPresent( existingJavaPackage -> {
                Optional< JavaClass > optionalExistingJavaClass =
                    existingJavaPackage.getIncludedClass( stringClass );
                if( !optionalExistingJavaClass.isPresent() )
                {
                    JavaClass newJavaClass = new JavaClass( stringClass );
                    jsonInterpreter.putJavaApiElementToCorrectList( newJavaClass, currentApiElement );
                    existingJavaPackage.addClass( newJavaClass );
                }
                optionalExistingJavaClass.ifPresent( existingJavaClass -> {
                    existingJavaPackage.addClass( existingJavaClass );
                    jsonInterpreter.putJavaApiElementToCorrectList( existingJavaClass, currentApiElement );
                } );
            } );

            if( !optionalExistingJavaPackage.isPresent() )
            {
                JavaPackage newJavaPackage = new JavaPackage( stringPackage );
                packagesList.add( newJavaPackage );
                JavaClass newJavaClass = new JavaClass( stringClass );
                newJavaPackage.addClass( newJavaClass );
                jsonInterpreter.putJavaApiElementToCorrectList( newJavaClass, currentApiElement );
            }
        } );
    }

    private String getJavaClass( String className )
    {
        String[] splitedName = className.split( "\\." );
        return splitedName[ splitedName.length - 2 ] + "." + splitedName[ splitedName.length - 1 ];
    }

    private String getJavaPackage( String packageName )
    {
        int lastIndex = packageName.lastIndexOf( "." );
        packageName = packageName.substring( 0, lastIndex );
        lastIndex = packageName.lastIndexOf( "." );
        packageName = packageName.substring( 0, lastIndex );

        return packageName;
    }

}
