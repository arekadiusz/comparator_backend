package de.psi.pjf.apiComparator.processingdata;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class JavaPackage
{
    private final String name;
    private Set< JavaClass > classes = new HashSet<>();

    public JavaPackage( String name )
    {
        super();
        this.name = name;
    }

    public void addClass( JavaClass aClass )
    {
        classes.add( aClass );
    }

    public String getName()
    {
        return name;
    }

    public Optional< JavaClass > getIncludedClass( String stringClass )
    {
        return classes.stream().filter( c -> c.getName().equals( stringClass ) ).findAny();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        JavaPackage other = (JavaPackage)obj;
        if( name == null )
        {
            if( other.name != null )
                return false;
        }
        else if( !name.equals( other.name ) )
            return false;
        return true;
    }
}
