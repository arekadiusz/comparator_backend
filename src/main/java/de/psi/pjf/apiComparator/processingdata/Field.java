//******************************************************************
//                                                                 
//  Field.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class Field extends AbstractJavaApiElement
{
    private final String name;
    private final String type;
    private final boolean isStatic;

    public Field( String className, String name, String type, boolean isStatic )
    {
        super( className );
        this.name = name;
        this.type = type;
        this.isStatic = isStatic;
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    public boolean isStatic()
    {
        return isStatic;
    }

    @Override
    public String toString()
    {
        String fieldType = isStatic ? LineType.STATIC_FIELD.toString() : LineType.FIELD.toString();
        return getClassName() + CsvBuffer.DEFAULT_SEPARATOR + CsvBuffer.DEFAULT_SEPARATOR
            + CsvBuffer.DEFAULT_SEPARATOR + CsvBuffer.DEFAULT_SEPARATOR + fieldType
            + CsvBuffer.DEFAULT_SEPARATOR + type + CsvBuffer.DEFAULT_SEPARATOR + name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (isStatic ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
            return true;
        if( !super.equals( obj ) )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        Field other = (Field)obj;
        if( isStatic != other.isStatic )
            return false;
        if( name == null )
        {
            if( other.name != null )
                return false;
        }
        else if( !name.equals( other.name ) )
            return false;
        if( type == null )
        {
            if( other.type != null )
                return false;
        }
        else if( !type.equals( other.type ) )
            return false;
        return true;
    }
}
