package de.psi.pjf.apiComparator.processingdata;

public abstract class AbstractJavaApiElement
{
    private final String className;

    public AbstractJavaApiElement( String aClassName )
    {
        className = aClassName;
    }

    public String getClassName()
    {
        return className;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        AbstractJavaApiElement other = (AbstractJavaApiElement)obj;
        if( className == null )
        {
            if( other.className != null )
                return false;
        }
        else if( !className.equals( other.className ) )
            return false;
        return true;
    }
}
