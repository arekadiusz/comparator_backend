//******************************************************************
//                                                                 
//  PjfComponentApi.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import java.util.ArrayList;
import java.util.List;

public class PjfComponentApi
{
    // :TODO
    // private String name;
    // private String version;
    // private List< String > listOfJars;

    private final List< Method > methodsList;
    private final List< Field > fieldsList;
    private final List< Constructor > constructorsList;

    public PjfComponentApi( List< Method > methodsList, List< Field > fieldsList,
        List< Constructor > constructorsList )
    {
        super();
        this.methodsList = methodsList;
        this.fieldsList = fieldsList;
        this.constructorsList = constructorsList;
    }

    public List< Field > getFieldsList()
    {
        return new ArrayList<>( fieldsList );
    }

    public List< Constructor > getConstructorsList()
    {
        return new ArrayList<>( constructorsList );
    }

    public List< Method > getMethodsList()
    {
        return new ArrayList<>( methodsList );
    }

}
