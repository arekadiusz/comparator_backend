//******************************************************************
//                                                                 
//  FieldsToObjectWriterStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import java.util.regex.Pattern;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.csv.CsvBuffer;


public class LineToFieldParserStrategy implements LineToObjectParserStrategyIf< Field >
{

    @Override
    public Field parse( String line )
    {
        String[] cuttedLine = line.split( Pattern.quote( String.valueOf( CsvBuffer.DEFAULT_SEPARATOR ) ) );
        Field field = null;

        if( cuttedLine[ 4 ].contains( LineType.STATIC_FIELD.toString() ) )
        {
            field = new Field( cuttedLine[ 0 ], cuttedLine[ 6 ], cuttedLine[ 5 ], true );
        }
        else
        {
            field = new Field( cuttedLine[ 0 ], cuttedLine[ 6 ], cuttedLine[ 5 ], false );

        }
        return field;
    }

}
