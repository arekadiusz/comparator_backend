package de.psi.pjf.apiComparator.processingdata;

import java.util.ArrayList;
import java.util.List;

public class JavaClass
{
    private final String name;
    private List< Method > addedMethods = new ArrayList<>();
    private List< Method > removedMethods = new ArrayList<>();
    private List< Field > addedFields = new ArrayList<>();
    private List< Field > removedFields = new ArrayList<>();
    private List< Constructor > addedConstructors = new ArrayList<>();
    private List< Constructor > removedConstructors = new ArrayList<>();

    public JavaClass( String aJavaClass )
    {
        name = aJavaClass;
    }

    public String getName()
    {
        return name;
    }

    public void addAddedMethod( Method aMethod )
    {
        addedMethods.add( aMethod );
    }

    public void addRemovedMethod( Method aMethod )
    {
        removedMethods.add( aMethod );
    }

    public void addAddedField( Field aField )
    {
        addedFields.add( aField );
    }

    public void addRemovedField( Field aField )
    {
        removedFields.add( aField );
    }

    public void addAddedConstructor( Constructor aConstructor )
    {
        addedConstructors.add( aConstructor );
    }

    public void addRemovedConstructor( Constructor aConstructor )
    {
        removedConstructors.add( aConstructor );
    }

}
