//******************************************************************
//                                                                 
//  LineToObjectWriterStrategyIf.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

public interface LineToObjectParserStrategyIf< T >
{
    public T parse( String aLine );
}
