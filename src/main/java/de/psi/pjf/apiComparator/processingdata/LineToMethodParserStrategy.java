//******************************************************************
//                                                                 
//  MethodsToListWriterStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import java.util.regex.Pattern;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class LineToMethodParserStrategy implements LineToObjectParserStrategyIf< Method >
{

    @Override
    public Method parse( String line )
    {
        String[] cuttedLine = line.split( Pattern.quote( String.valueOf( CsvBuffer.DEFAULT_SEPARATOR ) ) );
        Method method = null;

        if( cuttedLine[ 4 ].contains( LineType.ABSTRACT_METHOD.toString() ) )
        {
            method =
                new Method( cuttedLine[ 0 ], cuttedLine[ 1 ], cuttedLine[ 2 ], cuttedLine[ 3 ], true, false );

        }
        else if( cuttedLine[ 4 ].contains( LineType.STATIC_METHOD.toString() ) )
        {
            method =
                new Method( cuttedLine[ 0 ], cuttedLine[ 1 ], cuttedLine[ 2 ], cuttedLine[ 3 ], false, true );
        }
        else
        {
            method = new Method( cuttedLine[ 0 ], cuttedLine[ 1 ], cuttedLine[ 2 ], cuttedLine[ 3 ], false,
                false );
        }

        return method;
    }

}
