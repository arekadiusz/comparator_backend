//******************************************************************
//                                                                 
//  Method.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class Method extends AbstractJavaApiElement
{
    private final String name;
    private final String parametersList;
    private final String returnType;
    private final boolean isAbstract;
    private final boolean isStatic;

    public Method( String className, String name, String parametersList, String returnType,
        boolean isAbstract, boolean isStatic )
    {
        super( className );
        this.name = name;
        this.parametersList = parametersList;
        this.returnType = returnType;
        this.isAbstract = isAbstract;
        this.isStatic = isStatic;
    }

    public String getName()
    {
        return name;
    }

    public String getParametersList()
    {
        return parametersList;
    }

    public String getReturnType()
    {
        return returnType;
    }

    public boolean isAbstract()
    {
        return isAbstract;
    }

    public boolean isStatic()
    {
        return isStatic;
    }

    @Override
    public String toString()
    {
        String methodType = isStatic ? LineType.STATIC_METHOD.toString()
            : isAbstract ? LineType.ABSTRACT_METHOD.toString() : LineType.METHOD.toString();

        return getClassName() + CsvBuffer.DEFAULT_SEPARATOR + name + CsvBuffer.DEFAULT_SEPARATOR
            + parametersList + CsvBuffer.DEFAULT_SEPARATOR + returnType + CsvBuffer.DEFAULT_SEPARATOR
            + methodType + CsvBuffer.DEFAULT_SEPARATOR + CsvBuffer.DEFAULT_SEPARATOR;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (isAbstract ? 1231 : 1237);
        result = prime * result + (isStatic ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parametersList == null) ? 0 : parametersList.hashCode());
        result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
            return true;
        if( !super.equals( obj ) )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        Method other = (Method)obj;
        if( isAbstract != other.isAbstract )
            return false;
        if( isStatic != other.isStatic )
            return false;
        if( name == null )
        {
            if( other.name != null )
                return false;
        }
        else if( !name.equals( other.name ) )
            return false;
        if( parametersList == null )
        {
            if( other.parametersList != null )
                return false;
        }
        else if( !parametersList.equals( other.parametersList ) )
            return false;
        if( returnType == null )
        {
            if( other.returnType != null )
                return false;
        }
        else if( !returnType.equals( other.returnType ) )
            return false;
        return true;
    }

}
