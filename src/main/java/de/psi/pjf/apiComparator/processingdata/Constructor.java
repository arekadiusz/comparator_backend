//******************************************************************
//                                                                 
//  Constructor.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class Constructor extends AbstractJavaApiElement
{
    private final String parametersList;

    public Constructor( String className, String parametersList )
    {
        super( className );
        this.parametersList = parametersList;
    }

    public String getParametersList()
    {
        return parametersList;
    }

    @Override
    public String toString()
    {
        String name = getClassName().replace( ".class", "" );

        return getClassName() + CsvBuffer.DEFAULT_SEPARATOR + name + CsvBuffer.DEFAULT_SEPARATOR
            + parametersList + CsvBuffer.DEFAULT_SEPARATOR + CsvBuffer.DEFAULT_SEPARATOR
            + LineType.CONSTRUCTOR + CsvBuffer.DEFAULT_SEPARATOR + CsvBuffer.DEFAULT_SEPARATOR;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((parametersList == null) ? 0 : parametersList.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
            return true;
        if( !super.equals( obj ) )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        Constructor other = (Constructor)obj;
        if( parametersList == null )
        {
            if( other.parametersList != null )
                return false;
        }
        else if( !parametersList.equals( other.parametersList ) )
            return false;
        return true;
    }

}
