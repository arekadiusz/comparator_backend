//******************************************************************
//                                                                 
//  ConstructorToObjectWriterStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import de.psi.pjf.apiComparator.csv.CsvBuffer;

import java.util.regex.Pattern;


public class LineToConstructorParserStrategy implements LineToObjectParserStrategyIf< Constructor >
{

    @Override
    public Constructor parse( String line )
    {
        String[] cuttedLine = line.split( Pattern.quote( String.valueOf( CsvBuffer.DEFAULT_SEPARATOR ) ) );

        Constructor constructor = new Constructor( cuttedLine[ 0 ], cuttedLine[ 2 ] );
        return constructor;

    }

}
