//******************************************************************
//                                                                 
//  LineType.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

public enum LineType
{
    METHOD, STATIC_METHOD, ABSTRACT_METHOD, FIELD, STATIC_FIELD, CONSTRUCTOR, DISMISS;
}
