//******************************************************************
//                                                                 
//  FieldParserStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;


import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class FieldParserStrategy implements LineParserStrategyIf
{
    private final LineType lineType;

    public FieldParserStrategy()
    {
        lineType = LineType.FIELD;
    }

    public FieldParserStrategy( LineType lineType )
    {
        this.lineType = lineType;
    }

    @Override
    public String parse( String aLine )
    {
        String line = aLine.trim().replace( ";", "" );
        String[] splitedLine = line.split( " " );

        String fieldName = splitedLine[ splitedLine.length - 1 ];
        String fieldType = splitedLine[ splitedLine.length - 2 ];

        StringBuilder sbField = new StringBuilder();
        sbField.append( "" );
        sbField.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbField.append( "" );
        sbField.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbField.append( "" );
        sbField.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbField.append( lineType );
        sbField.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbField.append( fieldType );
        sbField.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbField.append( fieldName );

        return sbField.toString();

    }

}
