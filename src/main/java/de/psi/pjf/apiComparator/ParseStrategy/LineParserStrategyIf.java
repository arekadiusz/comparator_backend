//******************************************************************
//                                                                 
//  LineRecognizerIf.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

public interface LineParserStrategyIf
{
    public String parse( String aLine );
}
