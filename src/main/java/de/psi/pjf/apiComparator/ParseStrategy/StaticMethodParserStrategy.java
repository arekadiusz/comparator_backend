//******************************************************************
//                                                                 
//  StaticMethodParserStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

public class StaticMethodParserStrategy extends MethodParserStrategy
{
    public StaticMethodParserStrategy()
    {
        super( LineType.STATIC_METHOD );
    }
}
