//******************************************************************
//                                                                 
//  MethodParserStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;


import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class MethodParserStrategy implements LineParserStrategyIf
{
    private final LineType lineType;

    public MethodParserStrategy()
    {
        lineType = LineType.METHOD;
    }

    public MethodParserStrategy( LineType lineType )
    {
        this.lineType = lineType;
    }

    @Override
    public String parse( String aLine )
    {
        int indexOfThrows = aLine.trim().indexOf( "throws" );
        String line;
        if( indexOfThrows == -1 )
        {
            line = aLine.trim().replace( ";", "" );
        }
        else
        {
            line = aLine.trim().replace( ";", "" ).substring( 0, indexOfThrows - 1 );
        }

        String[] splitedLine = line.split( "\\(" );

        String methodFullName = splitedLine[ 0 ];
        String parameter = "";

        if( splitedLine.length < 2 || splitedLine[ 1 ].contentEquals( ")" ) )
        {
            parameter = "none";
        }
        else
        {
            parameter = splitedLine[ 1 ];
        }

        String parsedParameter = parameter.replace( ")", "" ).replace( ", ", " & " ).replace( "...", "" );

        String[] splitedMethodName = methodFullName.split( " " );
        String returnType = splitedMethodName[ splitedMethodName.length - 2 ];
        String methodName = splitedMethodName[ splitedMethodName.length - 1 ];

        StringBuilder sbMethod = new StringBuilder();
        sbMethod.append( methodName );
        sbMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbMethod.append( parsedParameter );
        sbMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbMethod.append( returnType );
        sbMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbMethod.append( lineType );
        sbMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbMethod.append( "" );
        sbMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbMethod.append( "" );

        return sbMethod.toString();
    }
}
