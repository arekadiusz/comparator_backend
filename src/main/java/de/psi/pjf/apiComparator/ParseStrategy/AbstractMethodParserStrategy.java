//******************************************************************
//                                                                 
//  AbstractMethodParserStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;


import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class AbstractMethodParserStrategy implements LineParserStrategyIf
{

    @Override
    public String parse( String aLine )
    {
        int indexOfThrows = aLine.trim().indexOf( "throws" );
        String line;
        if( indexOfThrows == -1 )
        {
            line = aLine.trim().replace( ";", "" );
        }
        else
        {
            line = aLine.trim().replace( ";", "" ).substring( 0, indexOfThrows - 1 );
        }

        String[] splitedLine = line.split( "\\(" );

        String abstractMethodFullName = splitedLine[ 0 ];
        String parameter = "";

        if( splitedLine[ 1 ].contentEquals( ")" ) )
        {
            parameter = "none";
        }
        else
        {
            parameter = splitedLine[ 1 ];
        }

        String parsedParameter = parameter.replace( ")", "" ).replace( ", ", " & " ).replace( "...", "" );

        String[] splitedAbstractMethodFullName = abstractMethodFullName.split( " " );
        String returnType = splitedAbstractMethodFullName[ 2 ];
        String abstractMethodName = splitedAbstractMethodFullName[ splitedAbstractMethodFullName.length - 1 ];

        StringBuilder sbAbstractMethod = new StringBuilder();
        sbAbstractMethod.append( abstractMethodName );
        sbAbstractMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbAbstractMethod.append( parsedParameter );
        sbAbstractMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbAbstractMethod.append( returnType );
        sbAbstractMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbAbstractMethod.append( LineType.ABSTRACT_METHOD );
        sbAbstractMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbAbstractMethod.append( "" );
        sbAbstractMethod.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbAbstractMethod.append( "" );

        return sbAbstractMethod.toString();
    }

}
