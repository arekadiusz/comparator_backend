//******************************************************************
//                                                                 
//  LineRecognizer.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

public class LineRecognizer
{
    public LineType recognizeLineType( String aLine )
    {
        String[] splitedLine = aLine.trim().split( " " );
        if( splitedLine[ 1 ].equals( "abstract" ) )
        {
            return LineType.ABSTRACT_METHOD;
        }
        else if( splitedLine.length == 2 )
        {
            return LineType.CONSTRUCTOR;
        }
        else if( aLine.endsWith( ");" ) || isMethodWithThrows( splitedLine ) )
        {
            if( splitedLine[ 1 ].equals( "static" ) )
            {
                return LineType.STATIC_METHOD;
            }
            else
            {
                return LineType.METHOD;
            }
        }
        else if( !(aLine.endsWith( ");" )) )
        {
            if( splitedLine[ 1 ].equals( "static" ) )
            {
                return LineType.STATIC_FIELD;
            }
            else
            {
                return LineType.FIELD;
            }
        }
        else
        {
            return LineType.DISMISS;
        }

    }

    private boolean isMethodWithThrows( String[] aSplitedLine )
    {
        for( String string : aSplitedLine )
        {
            if( string.equals( "throws" ) )
                return true;

        }
        return false;

    }
}
