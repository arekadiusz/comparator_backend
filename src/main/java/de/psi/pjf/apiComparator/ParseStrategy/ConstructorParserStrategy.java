//******************************************************************
//                                                                 
//  ConstructorParserStrategy.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;


import de.psi.pjf.apiComparator.csv.CsvBuffer;

public class ConstructorParserStrategy implements LineParserStrategyIf
{

    @Override
    public String parse( String aLine )
    {
        String line = aLine.trim().replace( ");", "" );

        String[] splitedLine = line.split( "\\(" );
        String constructorFullName = splitedLine[ 0 ];
        String parameter = "";

        if( splitedLine.length < 2 || splitedLine[ 1 ].contentEquals( ")" ) )
        {
            parameter = "none";
        }
        else
        {
            parameter = splitedLine[ 1 ];
        }

        String parsedParameter = parameter.replace( ")", "" ).replace( ", ", " & " ).replace( "...", "" );

        String[] splitedConstructorName = constructorFullName.split( " " );
        String constructorName = splitedConstructorName[ 1 ];

        StringBuilder sbConstructor = new StringBuilder();
        sbConstructor.append( constructorName );
        sbConstructor.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbConstructor.append( parsedParameter );
        sbConstructor.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbConstructor.append( "" );
        sbConstructor.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbConstructor.append( LineType.CONSTRUCTOR );
        sbConstructor.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbConstructor.append( "" );
        sbConstructor.append( CsvBuffer.DEFAULT_SEPARATOR );
        sbConstructor.append( "" );

        return sbConstructor.toString();
    }

}
