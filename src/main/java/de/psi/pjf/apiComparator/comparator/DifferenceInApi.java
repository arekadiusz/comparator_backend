//******************************************************************
//                                                                 
//  DifferenceInApi.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.comparator;

import java.util.List;

import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;

public class DifferenceInApi< T >
{
    // :TODO
    // private String componentName;
    // private String oldVersion;
    // private String newVersion;

    private final List< Method > addedMethods;
    private final List< Method > removedMethods;
    private final List< Field > addedFields;
    private final List< Field > removedFields;
    private final List< Constructor > addedConstructors;
    private final List< Constructor > removedConstructors;

    public DifferenceInApi( List< Method > addedMethods, List< Method > removedMethods,
        List< Field > addedFields, List< Field > removedFields, List< Constructor > addedConstructors,
        List< Constructor > removedConstructors )
    {
        super();
        this.addedMethods = addedMethods;
        this.removedMethods = removedMethods;
        this.addedFields = addedFields;
        this.removedFields = removedFields;
        this.addedConstructors = addedConstructors;
        this.removedConstructors = removedConstructors;
    }

    public List< Method > getAddedMethods()
    {
        return addedMethods;
    }

    public List< Method > getRemovedMethods()
    {
        return removedMethods;
    }

    public List< Field > getAddedFields()
    {
        return addedFields;
    }

    public List< Field > getRemovedFields()
    {
        return removedFields;
    }

    public List< Constructor > getAddedConstructors()
    {
        return addedConstructors;
    }

    public List< Constructor > getRemovedConstructors()
    {
        return removedConstructors;
    }

}
