//******************************************************************
//                                                                 
//  ApiComparator.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.comparator;

import java.util.ArrayList;
import java.util.List;

import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

public class ApiComparator
{

    private final PjfComponentApi oldApi;
    private final PjfComponentApi newApi;

    private DifferenceInApi< Method > methodDiff;
    private DifferenceInApi< Field > fieldDiff;
    private DifferenceInApi< Constructor > constructorDiff;

    public ApiComparator( PjfComponentApi oldApi, PjfComponentApi newApi )
    {
        super();
        this.oldApi = oldApi;
        this.newApi = newApi;
    }

    public DifferenceInApi< Method > getMethodDiff()
    {
        if( methodDiff == null )
        {
            List< Method > addedMethods = new ArrayList<>();
            List< Method > removedMethods = new ArrayList<>();
            for( Method methodNew : newApi.getMethodsList() )
            {
                if( !oldApi.getMethodsList().contains( methodNew ) )
                {
                    addedMethods.add( methodNew );
                }
            }
            for( Method methodOld : oldApi.getMethodsList() )
            {
                if( !newApi.getMethodsList().contains( methodOld ) )
                {
                    removedMethods.add( methodOld );
                }
            }

            methodDiff =
                new DifferenceInApi< Method >( addedMethods, removedMethods, null, null, null, null );
        }
        return methodDiff;
    }

    public DifferenceInApi< Field > getFieldDiff()
    {
        if( fieldDiff == null )
        {
            List< Field > addedFields = new ArrayList<>();
            List< Field > removedFields = new ArrayList<>();
            for( Field fieldNew : newApi.getFieldsList() )
            {
                if( !oldApi.getFieldsList().contains( fieldNew ) )
                {
                    addedFields.add( fieldNew );
                }
            }
            for( Field fieldOld : oldApi.getFieldsList() )
            {
                if( !newApi.getFieldsList().contains( fieldOld ) )
                {
                    removedFields.add( fieldOld );
                }
            }

            fieldDiff = new DifferenceInApi< Field >( null, null, addedFields, removedFields, null, null );
        }
        return fieldDiff;
    }

    public DifferenceInApi< Constructor > getConstructorDiff()
    {
        if( constructorDiff == null )
        {
            List< Constructor > addedConstructors = new ArrayList<>();
            List< Constructor > removedConstructors = new ArrayList<>();
            for( Constructor constructorNew : newApi.getConstructorsList() )
            {
                if( !oldApi.getConstructorsList().contains( constructorNew ) )
                {
                    addedConstructors.add( constructorNew );
                }
            }
            for( Constructor constructorOld : oldApi.getConstructorsList() )
            {
                if( !newApi.getConstructorsList().contains( constructorOld ) )
                {
                    removedConstructors.add( constructorOld );
                }
            }

            constructorDiff = new DifferenceInApi< Constructor >( null, null, null, null, addedConstructors,
                removedConstructors );
        }

        return constructorDiff;
    }

}
