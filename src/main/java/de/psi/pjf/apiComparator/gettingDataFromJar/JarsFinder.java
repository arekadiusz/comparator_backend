//******************************************************************
//                                                                 
//  JarsFinder.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JarsFinder
{
    public List< File > findAllJarsFromDirectory( String aDirectory )
    {
        File direcotry = new File( aDirectory );
        File[] listFiles = direcotry.listFiles();

        List< File > listAllJars = new ArrayList< File >();
        for( File file : listFiles )
        {
            if( file.getName().endsWith( "jar" ) )
            {
                listAllJars.add( file );

            }
        }
        return listAllJars;
    }

}
