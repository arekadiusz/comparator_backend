//******************************************************************
//                                                                 
//  MethodsListFromClass.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MethodsListFromClass
{
    public List< String > getMethodsFromClass( File aJarFile, String aClassName )
    {
        Runtime rt = Runtime.getRuntime();

        Process process = null;
        try
        {
            process = rt.exec( "javap -cp " + aJarFile.getAbsolutePath() + " -protected " + aClassName );
        }
        catch( IOException e1 )
        {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader( new InputStreamReader( process.getInputStream() ) );
        String readLine = "";

        List< String > listMethodsFromJar = new ArrayList< String >();
        try
        {
            while( (readLine = reader.readLine()) != null )
            {
                if( readLine.endsWith( ";" ) )
                {
                    // String[] tab = readLine.split( "/" );
                    // listClassesFromJar.add( tab[ tab.length - 1 ] );

                    // String name = readLine
                    listMethodsFromJar.add( readLine );
                }
            }
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        return listMethodsFromJar;

    }
}
