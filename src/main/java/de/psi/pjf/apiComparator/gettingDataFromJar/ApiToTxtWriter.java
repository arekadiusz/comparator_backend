//******************************************************************
//                                                                 
//  ApiToTxtWriter.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ApiToTxtWriter
{
    public void write( String directory ) throws FileNotFoundException, UnsupportedEncodingException
    {

        JarsFinder jarsFinder = new JarsFinder();
        List< File > jarsList = jarsFinder.findAllJarsFromDirectory( directory );

        PrintWriter writer = new PrintWriter( "APIfile.txt", "UTF-8" );

        for( File file : jarsList )
        {
            ClassesListFromJar classesListFromJar = new ClassesListFromJar();
            List< String > classesList = classesListFromJar.getClassesFromJar( file );
            String[] jarName = file.toString().split( "\\\\" );
            writer.println( jarName[ jarName.length - 1 ] );

            for( String aClassName : classesList )
            {
                MethodsListFromClass methodsListFromClass = new MethodsListFromClass();
                List< String > methodsListFromFirstJar =
                    methodsListFromClass.getMethodsFromClass( file, aClassName );
                writer.println( aClassName + ".class" );

                for( String method : methodsListFromFirstJar )
                {
                    writer.println( method );
                }
                writer.println( "" );
            }
        }
        writer.close();
    }
}
