//******************************************************************
//                                                                 
//  ClassListFromJar.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ClassesListFromJar
{
    // private static final Logger LOGGER = LogManager.getLogger( ApiToTxtWriter.class.getName() );

    public List< String > getClassesFromJar( File file )
    {
        Runtime rt = Runtime.getRuntime();

        Process process = null;
        try
        {
            process = rt.exec( "jar -tf " + file.getAbsolutePath() );
        }
        catch( IOException e1 )
        {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader( new InputStreamReader( process.getInputStream() ) );
        String readLine = "";

        List< String > listClassesFromJar = new ArrayList< String >();
        try
        {
            while( (readLine = reader.readLine()) != null )
            {
                if( readLine.endsWith( "class" ) )
                {
                    // String[] tab = readLine.split( "/" );
                    // listClassesFromJar.add( tab[ tab.length - 1 ] );

                    String name = readLine.replace( "/", "." ).replace( ".class", "" );
                    listClassesFromJar.add( name );
                }
            }
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        return listClassesFromJar;

    }

}
