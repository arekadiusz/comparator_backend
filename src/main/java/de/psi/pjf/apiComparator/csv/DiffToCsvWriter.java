//******************************************************************
//                                                                 
//  DiffToCsvWriter.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.csv;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.psi.pjf.apiComparator.comparator.ApiComparator;
import de.psi.pjf.apiComparator.comparator.DifferenceInApi;
import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

import static de.psi.pjf.apiComparator.csv.CsvBuffer.*;

public class DiffToCsvWriter
{
    public void write( String firstApiPath, String secondApiPath ) throws IOException
    {
        File oldApiFile = new File( firstApiPath );
        CsvFileReader csvoldApiFileReader = new CsvFileReader();
        PjfComponentApi oldApi = csvoldApiFileReader.loadLinesFromFile( oldApiFile );

        File newApiFile = new File( secondApiPath );
        CsvFileReader csvnewApiFileReader = new CsvFileReader();
        PjfComponentApi newApi = csvnewApiFileReader.loadLinesFromFile( newApiFile );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );
        DifferenceInApi< Method > differenceInApiMethod = apiComparator.getMethodDiff();
        DifferenceInApi< Field > differenceInApiField = apiComparator.getFieldDiff();
        DifferenceInApi< Constructor > differenceInApiConstructor = apiComparator.getConstructorDiff();

        ApiToCsvWriter apiToCsvWriter = new ApiToCsvWriter();

        File inputFile = new File( firstApiPath );
        String parentPath = inputFile.getParent();
        FileWriter writer = new FileWriter( parentPath + "\\diffAPI.csv" );

        writer.write( apiToCsvWriter.createHeader() );
        writer.write( "ADDED_METHODS" );
        writer.write( NEW_LINE );
        for( Method addedMethod : differenceInApiMethod.getAddedMethods() )
        {
            writer.write( addedMethod.toString() );
            writer.write( NEW_LINE );
        }

        writer.write( "REMOVED_METHODS" );
        writer.write( NEW_LINE );
        for( Method removedMethod : differenceInApiMethod.getRemovedMethods() )
        {
            writer.write( removedMethod.toString() );
            writer.write( NEW_LINE );
        }
        writer.write( "ADDED_FIELDS" );
        writer.write( NEW_LINE );
        for( Field addedField : differenceInApiField.getAddedFields() )
        {
            writer.write( addedField.toString() );
            writer.write( NEW_LINE );
        }
        writer.write( "REMOVED_FIELDS" );
        writer.write( NEW_LINE );
        for( Field removedField : differenceInApiField.getRemovedFields() )
        {
            writer.write( removedField.toString() );
            writer.write( NEW_LINE );
        }
        writer.write( "ADDED_CONSTRUCTORS" );
        writer.write( NEW_LINE );
        for( Constructor addedConstructor : differenceInApiConstructor.getAddedConstructors() )
        {
            writer.write( addedConstructor.toString() );
            writer.write( NEW_LINE );
        }
        writer.write( "REMOVED_CONSTRUCTORS" );
        writer.write( NEW_LINE );
        for( Constructor removedConstructor : differenceInApiConstructor.getRemovedConstructors() )
        {
            writer.write( removedConstructor.toString() );
            writer.write( NEW_LINE );
        }
        writer.write( NEW_LINE );

        writer.flush();
        writer.close();

    }

}
