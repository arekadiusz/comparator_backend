//******************************************************************
//                                                                 
//  CsvReadLiner.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import de.psi.pjf.apiComparator.ParseStrategy.LineType;
import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.LineToConstructorParserStrategy;
import de.psi.pjf.apiComparator.processingdata.LineToFieldParserStrategy;
import de.psi.pjf.apiComparator.processingdata.LineToMethodParserStrategy;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

public class CsvFileReader extends CsvBuffer
{
    public PjfComponentApi loadLinesFromFile( File aCsvFile )
    {
        List< Method > methodsList = new ArrayList<>();
        List< Field > fieldsList = new ArrayList<>();
        List< Constructor > constructorsList = new ArrayList<>();
        PjfComponentApi ret = new PjfComponentApi( methodsList, fieldsList, constructorsList );

        BufferedReader br = null;
        String line = "";
        try
        {
            br = new BufferedReader( new FileReader( aCsvFile ) );

            while( (line = br.readLine()) != null )
            {
                String[] cuttedLine = line.split( Pattern.quote( String.valueOf( DEFAULT_SEPARATOR ) ) );

                if( cuttedLine.length < 2 || cuttedLine[ 0 ].equals( "class" )
                    || cuttedLine[ 0 ].endsWith( "jar" ) )
                {
                    continue;
                }

                // TODO: Header for component
                // else if( cuttedLine.length == 3 )
                // {
                // ret.setComponentName();
                // ret.setVersion();
                // }

                else if( cuttedLine[ 4 ].contains( LineType.METHOD.toString() ) )
                {
                    LineToMethodParserStrategy lineToMethodParserStrategy = new LineToMethodParserStrategy();
                    Method method = lineToMethodParserStrategy.parse( line );
                    methodsList.add( method );
                }
                else if( cuttedLine[ 4 ].contains( LineType.FIELD.toString() ) )
                {
                    LineToFieldParserStrategy LineToFieldParserStrategy = new LineToFieldParserStrategy();
                    Field field = LineToFieldParserStrategy.parse( line );
                    fieldsList.add( field );
                }
                else if( cuttedLine[ 4 ].contains( LineType.CONSTRUCTOR.toString() ) )
                {
                    LineToConstructorParserStrategy lineToConstructorParserStrategy =
                        new LineToConstructorParserStrategy();
                    Constructor constructor = lineToConstructorParserStrategy.parse( line );
                    constructorsList.add( constructor );
                }
            }
        }
        catch( FileNotFoundException e )
        {
            e.printStackTrace();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        finally
        {
            if( br != null )
            {
                try
                {
                    br.close();
                }
                catch( IOException e )
                {
                    e.printStackTrace();
                }
            }
        }

        return ret;
    }

}
