package de.psi.pjf.apiComparator.csv;

public abstract class CsvBuffer
{
    public static final char DEFAULT_SEPARATOR = '|';
    public static final String NEW_LINE = System.getProperty( "line.separator" );

}
