//******************************************************************
//                                                                 
//  ApiToCsvWriter.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.csv;


import de.psi.pjf.apiComparator.ParseStrategy.*;
import de.psi.pjf.apiComparator.gettingDataFromJar.ClassesListFromJar;
import de.psi.pjf.apiComparator.gettingDataFromJar.JarsFinder;
import de.psi.pjf.apiComparator.gettingDataFromJar.MethodsListFromClass;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class ApiToCsvWriter extends CsvBuffer
{

    // private final boolean readMethodFromInterfacesOnly = false;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void write(String directory ) throws IOException
    {
        JarsFinder jarsFinder = new JarsFinder();
        List<File> jarsList = jarsFinder.findAllJarsFromDirectory( directory );

        FileWriter writer = new FileWriter( directory + "\\api.csv" );

        // for( File file : jarsList )
        // {
        // String[] jarName = file.toString().split( "\\\\" );
        // writer.write( jarName[ jarName.length - 1 ] );
        // writer.write( NEW_LINE );
        // }

        writer.write( createHeader() );

        for( File file : jarsList )
        {
            ClassesListFromJar classesListFromJar = new ClassesListFromJar();
            List< String > classesList = classesListFromJar.getClassesFromJar( file );

            String[] jarName = file.toString().split( "\\\\" );
            writer.write( jarName[ jarName.length - 1 ] );
            writer.write( NEW_LINE );

            for( String aClassName : classesList )
            {
                MethodsListFromClass methodsListFromClass = new MethodsListFromClass();
                List< String > methodsListFromFirstJar =
                    methodsListFromClass.getMethodsFromClass( file, aClassName );
                StringBuilder sbClass = new StringBuilder();
                sbClass.append( aClassName );
                sbClass.append( ".class" );

                LineRecognizer recognizer = new LineRecognizer();

                for( String line : methodsListFromFirstJar )
                {
                    String unifiedObject = null;
                    switch( recognizer.recognizeLineType( line ) )
                    {
                        case FIELD:
                            unifiedObject = new FieldParserStrategy().parse( line );
                            break;
                        case STATIC_FIELD:
                            unifiedObject = new StaticFieldParserStrategy().parse( line );
                            break;
                        case CONSTRUCTOR:
                            unifiedObject = new ConstructorParserStrategy().parse( line );
                            break;
                        case METHOD:
                            // if(){
                            // continue;
                            // }
                            unifiedObject = new MethodParserStrategy().parse( line );
                            break;
                        case STATIC_METHOD:
                            unifiedObject = new StaticMethodParserStrategy().parse( line );
                            break;
                        case ABSTRACT_METHOD:
                            unifiedObject = new AbstractMethodParserStrategy().parse( line );
                            break;
                        case DISMISS:
                            break;
                    }

                    writer.write( sbClass.toString() );
                    writer.write( DEFAULT_SEPARATOR );
                    writer.write( unifiedObject );
                    writer.write( NEW_LINE );
                }
            }
            writer.flush();
        }
        writer.close();
    }

    public String createHeader()
    {
        StringBuilder sb = new StringBuilder();
        sb.append( "Class" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "Method" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "ParametersList" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "ReturnType" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "ENUM" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "FieldType" );
        sb.append( DEFAULT_SEPARATOR );
        sb.append( "FieldName" );

        sb.append( NEW_LINE );
        return sb.toString();
    }

}
