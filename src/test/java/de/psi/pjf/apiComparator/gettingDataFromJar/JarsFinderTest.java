//******************************************************************
//                                                                 
//  JarsFinderTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class JarsFinderTest
{
    @Test
    public void findAllJarsFromDirectoryTest()
    {
        List< File > list = new ArrayList< File >();
        JarsFinder jarsFinder = new JarsFinder();
        list = jarsFinder.findAllJarsFromDirectory( "src/test/resources/jars" );

        List< File > expectedList = new ArrayList< File >();
        expectedList.add( new File( "src/test/resources/jars/test.jar" ) );
        expectedList.add( new File( "src/test/resources/jars/test1.jar" ) );

        assertTrue( list.containsAll( expectedList ) && expectedList.containsAll( list ) );

    }
}
