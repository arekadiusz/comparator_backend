//******************************************************************
//                                                                 
//  MethodsListFromClassTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MethodsListFromClassTest
{
    @Test
    public void getMethodsFromClassTest()
    {
        List< String > list = new ArrayList< String >();
        MethodsListFromClass methodsListFromClass = new MethodsListFromClass();
        list = methodsListFromClass.getMethodsFromClass( new File( "src/test/resources/jars/test.jar" ),
            "de.psi.pjf.log.Log4JLogger" );

        List< String > expectedList = new ArrayList< String >();

        expectedList.add( "  public boolean isEnabled(de.psi.pjf.log.Level);" );
        expectedList.add(
            "  protected void doLog(de.psi.pjf.log.Level, java.lang.String, java.lang.String, java.lang.Throwable);" );

        assertTrue( list.containsAll( expectedList ) && expectedList.containsAll( list ) );

    }

    @Test
    public void getMethodsFromClassTest2()
    {
        List< String > list = new ArrayList< String >();
        MethodsListFromClass methodsListFromClass = new MethodsListFromClass();
        list = methodsListFromClass.getMethodsFromClass( new File( "src/test/resources/jars/test.jar" ),
            "de.psi.pjf.log.Log4JLoggerProvider" );

        List< String > expectedList = new ArrayList< String >();

        expectedList.add( "  public de.psi.pjf.log.Log4JLoggerProvider();" );
        expectedList.add( "  public de.psi.pjf.log.LogIf getLogger(java.lang.String);" );
        expectedList.add( "  public void putMdc(java.lang.String, java.lang.String);" );
        expectedList.add( "  public java.lang.String getMdc(java.lang.String);" );
        expectedList.add( "  public void removeMdc(java.lang.String);" );

        assertTrue( list.containsAll( expectedList ) && expectedList.containsAll( list ) );

    }

}
