//******************************************************************
//                                                                 
//  ClassListFromJarTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ClassesListFromJarTest
{
    @Test
    public void getClassesFromJarTest()
    {
        List< String > list = new ArrayList< String >();
        ClassesListFromJar classListFromJar = new ClassesListFromJar();
        list = classListFromJar.getClassesFromJar( new File( "src/test/resources/jars/test.jar" ) );

        List< String > expectedList = new ArrayList< String >();
        // expectedList.add( "Log4JLogger.class" );
        // expectedList.add( "Log4JLoggerProvider.class" );

        expectedList.add( "de.psi.pjf.log.Log4JLogger" );
        expectedList.add( "de.psi.pjf.log.Log4JLoggerProvider" );

        assertTrue( list.containsAll( expectedList ) && expectedList.containsAll( list ) );

    }

}
