//******************************************************************
//                                                                 
//  ApiToTxtWriterTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.gettingDataFromJar;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import org.junit.Test;

public class ApiToTxtWriterTest
{
    @Test
    public void writeTest() throws FileNotFoundException, UnsupportedEncodingException
    {
        ApiToTxtWriter apiToTxtWriter = new ApiToTxtWriter();
        apiToTxtWriter.write( "src/test/resources/jars" );

        @SuppressWarnings( "resource" )
        Scanner resultScanner = new Scanner( new File( "APIfile.txt" ) );
        @SuppressWarnings( "resource" )
        Scanner expectedResultScanner = new Scanner( new File( "src/test/resources/expectedFile.txt" ) );

        String resultLine;
        String expectedResultLine;
        while( resultScanner.hasNextLine() )
        {
            resultLine = resultScanner.nextLine();
            expectedResultLine = expectedResultScanner.nextLine();
            assertEquals( expectedResultLine, resultLine );
        }
        new File( "APIfile.txt" ).delete();
    }

}
