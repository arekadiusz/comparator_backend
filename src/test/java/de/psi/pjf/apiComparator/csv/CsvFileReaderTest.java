//******************************************************************
//                                                                 
//  CsvFileReaderTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.csv;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

public class CsvFileReaderTest
{
    File file = new File( "src/test/resources/ExpectedFile.csv" );

    @Test
    public void testAllMethodsLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Method > methodList = pjfComponentApi.getMethodsList().stream()
            .filter( method -> method.equals( method ) ).collect( Collectors.toList() );

        assertEquals( 65, methodList.size() );
    }

    @Test
    public void testMethodLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Method > methodList = pjfComponentApi.getMethodsList().stream()
            .filter( method -> !method.isAbstract() && !method.isStatic() ).collect( Collectors.toList() );

        assertEquals( 35, methodList.size() );
    }

    @Test
    public void testAbstractMethodLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Method > abstractMethodList = pjfComponentApi.getMethodsList().stream()
            .filter( method -> method.isAbstract() ).collect( Collectors.toList() );

        assertEquals( 25, abstractMethodList.size() );
    }

    @Test
    public void testStaticMethodLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Method > staticMethodList = pjfComponentApi.getMethodsList().stream()
            .filter( method -> method.isStatic() ).collect( Collectors.toList() );

        assertEquals( 5, staticMethodList.size() );
    }

    @Test
    public void testAllConstructorsLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Constructor > constructorList = pjfComponentApi.getConstructorsList().stream()
            .filter( constructor -> constructor.equals( constructor ) ).collect( Collectors.toList() );

        assertEquals( 4, constructorList.size() );
    }

    @Test
    public void testALlFieldsLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Field > fieldList = pjfComponentApi.getFieldsList().stream()
            .filter( field -> field.equals( field ) ).collect( Collectors.toList() );

        assertEquals( 7, fieldList.size() );
    }

    @Test
    public void testStaticFieldLoadLinesFromFile()
    {
        CsvFileReader csvFileReader = new CsvFileReader();
        PjfComponentApi pjfComponentApi = csvFileReader.loadLinesFromFile( file );

        List< Field > staticFieldList = pjfComponentApi.getFieldsList().stream()
            .filter( field -> field.isStatic() ).collect( Collectors.toList() );

        assertEquals( 7, staticFieldList.size() );
    }
}
