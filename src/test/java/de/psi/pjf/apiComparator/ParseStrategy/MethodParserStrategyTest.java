//******************************************************************
//                                                                 
//  MethodParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MethodParserStrategyTest
{
    @Test
    public void parseTest()
    {
        MethodParserStrategy methodParserStrategy = new MethodParserStrategy();
        String result =
            methodParserStrategy.parse( " public de.psi.pjf.log.LogIf getLogger(java.lang.String);" );

        assertEquals( "getLogger|java.lang.String|de.psi.pjf.log.LogIf|METHOD||", result );
    }

    @Test
    public void parseTest1()
    {
        MethodParserStrategy methodParserStrategy = new MethodParserStrategy();
        String result = methodParserStrategy.parse( " public T unwrap();" );

        assertEquals( "unwrap|none|T|METHOD||", result );
    }

    @Test
    public void parseTest2()
    {
        MethodParserStrategy methodParserStrategy = new MethodParserStrategy();
        String result = methodParserStrategy
            .parse( "  public abstract void error(java.lang.String, java.lang.Throwable);" );

        assertEquals( "error|java.lang.String & java.lang.Throwable|void|METHOD||", result );
    }

    @Test
    public void parseTest3()
    {
        MethodParserStrategy methodParserStrategy = new MethodParserStrategy();
        String result = methodParserStrategy.parse(
            "  public java.lang.reflect.Method getMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>...) throws java.lang.NoSuchMethodException;" );

        assertEquals(
            "getMethod|java.lang.Class<?> & java.lang.String & java.lang.Class<?>|java.lang.reflect.Method|METHOD||",
            result );
    }
}
