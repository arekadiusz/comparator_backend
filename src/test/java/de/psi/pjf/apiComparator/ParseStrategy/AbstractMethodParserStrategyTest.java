//******************************************************************
//                                                                 
//  AbstractMethodParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AbstractMethodParserStrategyTest
{
    @Test
    public void parseTest()
    {
        AbstractMethodParserStrategy abstractMethodParserStrategy = new AbstractMethodParserStrategy();
        String result = abstractMethodParserStrategy
            .parse( "  public abstract void trace(java.lang.String, java.lang.Throwable);" );

        assertEquals( "trace|java.lang.String & java.lang.Throwable|void|ABSTRACT_METHOD||", result );
    }

    @Test
    public void parseTest1()
    {
        AbstractMethodParserStrategy abstractMethodParserStrategy = new AbstractMethodParserStrategy();
        String result = abstractMethodParserStrategy.parse( "  public abstract boolean isTraceEnabled();" );

        assertEquals( "isTraceEnabled|none|boolean|ABSTRACT_METHOD||", result );
    }

    @Test
    public void parseTest2()
    {
        AbstractMethodParserStrategy abstractMethodParserStrategy = new AbstractMethodParserStrategy();
        String result = abstractMethodParserStrategy.parse(
            "  public abstract java.lang.reflect.Method getMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>...) throws java.lang.NoSuchMethodException;" );

        assertEquals(
            "getMethod|java.lang.Class<?> & java.lang.String & java.lang.Class<?>|java.lang.reflect.Method|ABSTRACT_METHOD||",
            result );
    }

}
