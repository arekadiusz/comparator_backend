//******************************************************************
//                                                                 
//  ConstructorParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConstructorParserStrategyTest
{
    @Test
    public void parseTest()
    {
        ConstructorParserStrategy constructorParserStrategy = new ConstructorParserStrategy();
        String result = constructorParserStrategy.parse( "  public de.psi.pjf.log.Log4JLoggerProvider();" );

        assertEquals( "de.psi.pjf.log.Log4JLoggerProvider|none||CONSTRUCTOR||", result );
    }

    @Test
    public void parseTest1()
    {
        ConstructorParserStrategy constructorParserStrategy = new ConstructorParserStrategy();
        String result =
            constructorParserStrategy.parse( "  public de.psi.pjf.log.Appender(T, java.lang.Class<T>);" );

        assertEquals( "de.psi.pjf.log.Appender|T & java.lang.Class<T>||CONSTRUCTOR||", result );
    }

    @Test
    public void parseTest2()
    {
        ConstructorParserStrategy constructorParserStrategy = new ConstructorParserStrategy();
        String result = constructorParserStrategy.parse( "  public de.psi.pjf.log.Appender(T);" );

        assertEquals( "de.psi.pjf.log.Appender|T||CONSTRUCTOR||", result );
    }

}
