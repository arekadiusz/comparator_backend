//******************************************************************
//                                                                 
//  FieldParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FieldParserStrategyTest
{
    @Test
    public void parseTest()
    {
        FieldParserStrategy fieldParserStrategy = new FieldParserStrategy();
        String result = fieldParserStrategy.parse( "  public final de.psi.pjf.log.Level FATAL;" );

        assertEquals( "|||FIELD|de.psi.pjf.log.Level|FATAL", result );
    }

}
