//******************************************************************
//                                                                 
//  LineRecognizerTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShouldRecognizeLineTest
{
    @Test
    public void recognizeLineTypeMethodTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result =
            lineRecognizer.recognizeLineType( "  public boolean isEnabled(de.psi.pjf.log.Level);" );

        assertEquals( LineType.METHOD, result );
    }

    @Test
    public void recognizeLineTypeStaticMethodTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result =
            lineRecognizer.recognizeLineType( "  public static java.lang.String get(java.lang.String);" );

        assertEquals( LineType.STATIC_METHOD, result );
    }

    @Test
    public void recognizeLineTypeStaticMethodWithThrowsTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result = lineRecognizer.recognizeLineType(
            "  public static java.lang.reflect.Method getMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>...) throws java.lang.NoSuchMethodException;" );

        assertEquals( LineType.STATIC_METHOD, result );
    }

    @Test
    public void recognizeLineTypeAbstractMethodTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result = lineRecognizer
            .recognizeLineType( "  public abstract void trace(java.lang.String, java.lang.Throwable);" );

        assertEquals( LineType.ABSTRACT_METHOD, result );
    }

    @Test
    public void recognizeLineTypeConstructorTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result = lineRecognizer.recognizeLineType( " public de.psi.pjf.log.Log4JLoggerProvider();" );

        assertEquals( LineType.CONSTRUCTOR, result );
    }

    @Test
    public void recognizeLineTypeFieldTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result =
            lineRecognizer.recognizeLineType( "  public final java.lang.String PROVIDER_CLASS_PROP;" );
        assertEquals( LineType.FIELD, result );
    }

    @Test
    public void recognizeLineTypeStaticFieldTest()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result =
            lineRecognizer.recognizeLineType( "  public static final de.psi.pjf.log.Level FATAL;" );
        assertEquals( LineType.STATIC_FIELD, result );
    }

    @Test
    public void recognizeLineTypeStaticField1Test()
    {
        LineRecognizer lineRecognizer = new LineRecognizer();
        LineType result =
            lineRecognizer.recognizeLineType( "  public static final java.lang.String PROVIDER_CLASS_PROP;" );
        assertEquals( LineType.STATIC_FIELD, result );
    }
    //
    // @Test
    // public void recognizeLineTypeDismissTest()
    // {
    // LineRecognizer lineRecognizer = new LineRecognizer();
    // LineType result = lineRecognizer.recognizeLineType( "test1.jar" );
    // assertEquals( LineType.DISMISS, result );
    // }

}
