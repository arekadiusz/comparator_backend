//******************************************************************
//                                                                 
//  StaticMethodParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StaticMethodParserStrategyTest
{
    @Test
    public void parseTest()
    {
        StaticMethodParserStrategy staticMethodParserStrategy = new StaticMethodParserStrategy();
        String result =
            staticMethodParserStrategy.parse( "  public static java.lang.String get(java.lang.String);" );

        assertEquals( "get|java.lang.String|java.lang.String|STATIC_METHOD||", result );
    }

    @Test
    public void parseTest2()
    {
        StaticMethodParserStrategy staticMethodParserStrategy = new StaticMethodParserStrategy();
        String result = staticMethodParserStrategy.parse(
            "  public static java.lang.reflect.Method getMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>...) throws java.lang.NoSuchMethodException;" );

        assertEquals(
            "getMethod|java.lang.Class<?> & java.lang.String & java.lang.Class<?>|java.lang.reflect.Method|STATIC_METHOD||",
            result );
    }

}
