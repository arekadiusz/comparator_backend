//******************************************************************
//                                                                 
//  StaticFieldParserStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.ParseStrategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StaticFieldParserStrategyTest
{
    @Test
    public void parseTest()
    {
        StaticFieldParserStrategy staticFieldParserStrategy = new StaticFieldParserStrategy();
        String result =
            staticFieldParserStrategy.parse( "  public static final de.psi.pjf.log.Level FATAL;" );

        assertEquals( "|||STATIC_FIELD|de.psi.pjf.log.Level|FATAL", result );
    }

}
