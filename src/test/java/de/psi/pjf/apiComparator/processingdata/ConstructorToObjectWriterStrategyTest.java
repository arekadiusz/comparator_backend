//******************************************************************
//                                                                 
//  ConstructorToObjectWriterStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConstructorToObjectWriterStrategyTest
{

    @Test
    public void testLineToObjectListWriter()
    {
        LineToConstructorParserStrategy lineToConstructorParserStrategy =
            new LineToConstructorParserStrategy();
        Constructor result = lineToConstructorParserStrategy.parse(
            "de.psi.pjf.log.LogConfigurationException.class|de.psi.pjf.log.LogConfigurationException|none||CONSTRUCTOR||" );

        assertEquals( "de.psi.pjf.log.LogConfigurationException.class", result.getClassName() );
        assertEquals( "none", result.getParametersList() );
    }

}
