//******************************************************************
//                                                                 
//  MethodsToObjectWriterStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MethodsToObjectWriterStrategyTest
{

    @Test
    public void testLineToObjectListWriter()
    {
        LineToMethodParserStrategy lineToMethodParserStrategy = new LineToMethodParserStrategy();
        Method result = lineToMethodParserStrategy
            .parse( "de.psi.pjf.log.Log4JLogger.class|isEnabled|de.psi.pjf.log.Level|boolean|METHOD||" );

        assertEquals( "de.psi.pjf.log.Log4JLogger.class", result.getClassName() );
        assertEquals( "isEnabled", result.getName() );
        assertEquals( "de.psi.pjf.log.Level", result.getParametersList() );
        assertEquals( "boolean", result.getReturnType() );
        assertEquals( false, result.isAbstract() );
        assertEquals( false, result.isStatic() );
    }

    @Test
    public void testAbstractLineToObjectListWriter()
    {
        LineToMethodParserStrategy lineToMethodParserStrategy = new LineToMethodParserStrategy();
        Method result = lineToMethodParserStrategy.parse(
            "de.psi.pjf.log.LogIf.class|trace|java.lang.String & java.lang.Throwable|void|ABSTRACT_METHOD||" );

        assertEquals( "de.psi.pjf.log.LogIf.class", result.getClassName() );
        assertEquals( "trace", result.getName() );
        assertEquals( "java.lang.String & java.lang.Throwable", result.getParametersList() );
        assertEquals( "void", result.getReturnType() );
        assertEquals( true, result.isAbstract() );
        assertEquals( false, result.isStatic() );

    }

    @Test
    public void testStaticLineToObjectListWriter()
    {
        LineToMethodParserStrategy lineToMethodParserStrategy = new LineToMethodParserStrategy();
        Method result = lineToMethodParserStrategy
            .parse( "de.psi.pjf.log.MDC.class|get|java.lang.String|java.lang.String|STATIC_METHOD||" );

        assertEquals( "de.psi.pjf.log.MDC.class", result.getClassName() );
        assertEquals( "get", result.getName() );
        assertEquals( "java.lang.String", result.getParametersList() );
        assertEquals( "java.lang.String", result.getReturnType() );
        assertEquals( false, result.isAbstract() );
        assertEquals( true, result.isStatic() );

    }

}
