//******************************************************************
//                                                                 
//  FieldsToObjectWriterStrategyTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.processingdata;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FieldsToObjectWriterStrategyTest
{

    @Test
    public void testLineToObjectListWriter()
    {
        LineToFieldParserStrategy lineToFieldParserStrategy = new LineToFieldParserStrategy();
        Field result = lineToFieldParserStrategy.parse(
            "de.psi.pjf.gui.base.model.actionsStorage.impl.ActionId.class||||FIELD|java.lang.String|ACTION_ID_EDEFAULT" );

        assertEquals( "de.psi.pjf.gui.base.model.actionsStorage.impl.ActionId.class", result.getClassName() );
        assertEquals( "ACTION_ID_EDEFAULT", result.getName() );
        assertEquals( "java.lang.String", result.getType() );
        assertEquals( false, result.isStatic() );
    }

    @Test
    public void testStaticLineToObjectListWriter()
    {
        LineToFieldParserStrategy lineToFieldParserStrategy = new LineToFieldParserStrategy();
        Field result = lineToFieldParserStrategy.parse(
            "de.psi.pjf.gui.base.model.actionsStorage.impl.ActionId.class||||STATIC_FIELD|java.lang.String|ACTION_ID_EDEFAULT" );

        assertEquals( "de.psi.pjf.gui.base.model.actionsStorage.impl.ActionId.class", result.getClassName() );
        assertEquals( "ACTION_ID_EDEFAULT", result.getName() );
        assertEquals( "java.lang.String", result.getType() );
        assertEquals( true, result.isStatic() );

    }

}
