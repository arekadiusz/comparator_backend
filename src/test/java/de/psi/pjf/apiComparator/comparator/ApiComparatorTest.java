//******************************************************************
//                                                                 
//  ApiComparatorTest.java                                               
//  Copyright 2018 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.apiComparator.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.Method;
import de.psi.pjf.apiComparator.processingdata.PjfComponentApi;

public class ApiComparatorTest
{
    @Test
    public void AddedMethodtest()
    {
        ArrayList< Method > newMethodInApi = new ArrayList<>();
        ArrayList< Method > oldMethodInApi = new ArrayList<>();

        Method methodInBothApi = new Method( "de.psi.pjf.log.Log4JLogger.class", "isEnabled",
            "de.psi.pjf.log.Level", "boolean", false, false );
        Method onlyInNewApi = new Method( "de.psi.pjf.log.Log4JLogger.class", "doLog",
            "de.psi.pjf.log.Level & java.lang.String & java.lang.String & java.lang.Throwable", "void", false,
            false );

        newMethodInApi.add( methodInBothApi );
        newMethodInApi.add( onlyInNewApi );

        oldMethodInApi.add( methodInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( newMethodInApi, null, null );
        PjfComponentApi oldApi = new PjfComponentApi( oldMethodInApi, null, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getMethodDiff().getAddedMethods().size() );
        assertEquals( onlyInNewApi, apiComparator.getMethodDiff().getAddedMethods().get( 0 ) );
    }

    @Test
    public void AddedMethodtest2()
    {
        ArrayList< Method > newMethodInApi = new ArrayList<>();
        ArrayList< Method > oldMethodInApi = new ArrayList<>();

        Method methodOne = new Method( "de.psi.pjf.log.Log4JLogger.class", "isEnabled",
            "de.psi.pjf.log.Level", "boolean", false, false );
        Method methodTwo =
            new Method( "de.psi.pjf.log.LogIf.class", "isTraceEnabled", "none", "boolean", true, false );
        Method methodThree = new Method( "de.psi.pjf.log.LogFactory.class", "getLogger", "java.lang.Class<?>",
            "de.psi.pjf.log.LogIf", false, true );

        newMethodInApi.add( methodOne );
        newMethodInApi.add( methodTwo );
        newMethodInApi.add( methodThree );

        PjfComponentApi newApi = new PjfComponentApi( newMethodInApi, null, null );
        PjfComponentApi oldApi = new PjfComponentApi( oldMethodInApi, null, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getMethodDiff().getAddedMethods().size() );
        assertEquals( methodOne, apiComparator.getMethodDiff().getAddedMethods().get( 0 ) );
        assertEquals( methodTwo, apiComparator.getMethodDiff().getAddedMethods().get( 1 ) );
        assertEquals( methodThree, apiComparator.getMethodDiff().getAddedMethods().get( 2 ) );
    }

    @Test
    public void RemovedMethodtest()
    {
        ArrayList< Method > newMethodInApi = new ArrayList<>();
        ArrayList< Method > oldMethodInApi = new ArrayList<>();

        Method methodInBothApi = new Method( "de.psi.pjf.log.Log4JLogger.class", "isEnabled",
            "de.psi.pjf.log.Level", "boolean", false, false );

        Method onlyInOldApi = new Method( "de.psi.pjf.log.Log4JLogger.class", "doLog",
            "de.psi.pjf.log.Level & java.lang.String & java.lang.String & java.lang.Throwable", "void", false,
            false );

        oldMethodInApi.add( methodInBothApi );
        oldMethodInApi.add( onlyInOldApi );

        newMethodInApi.add( methodInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( newMethodInApi, null, null );
        PjfComponentApi oldApi = new PjfComponentApi( oldMethodInApi, null, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getMethodDiff().getRemovedMethods().size() );
        assertEquals( onlyInOldApi, apiComparator.getMethodDiff().getRemovedMethods().get( 0 ) );
    }

    @Test
    public void RemovedMethodtest2()
    {
        ArrayList< Method > newMethodInApi = new ArrayList<>();
        ArrayList< Method > oldMethodInApi = new ArrayList<>();

        Method methodOne = new Method( "de.psi.pjf.log.Log4JLogger.class", "isEnabled",
            "de.psi.pjf.log.Level", "boolean", false, false );
        Method methodTwo =
            new Method( "de.psi.pjf.log.LogIf.class", "isTraceEnabled", "none", "boolean", true, false );
        Method methodThree = new Method( "de.psi.pjf.log.LogFactory.class", "getLogger", "java.lang.Class<?>",
            "de.psi.pjf.log.LogIf", false, true );

        oldMethodInApi.add( methodOne );
        oldMethodInApi.add( methodTwo );
        oldMethodInApi.add( methodThree );

        PjfComponentApi newApi = new PjfComponentApi( newMethodInApi, null, null );
        PjfComponentApi oldApi = new PjfComponentApi( oldMethodInApi, null, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getMethodDiff().getRemovedMethods().size() );
        assertEquals( methodOne, apiComparator.getMethodDiff().getRemovedMethods().get( 0 ) );
        assertEquals( methodTwo, apiComparator.getMethodDiff().getRemovedMethods().get( 1 ) );
        assertEquals( methodThree, apiComparator.getMethodDiff().getRemovedMethods().get( 2 ) );
    }

    @Test
    public void AddedFieldtest()
    {
        ArrayList< Field > newFieldInApi = new ArrayList<>();
        ArrayList< Field > oldFieldInApi = new ArrayList<>();

        Field fieldInBothApi =
            new Field( "de.psi.pjf.log.Level.class", "FATAL", "de.psi.pjf.log.Level", true );
        Field onlyInNewApi = new Field( "de.psi.pjf.log.Level.class", "DEBUG", "de.psi.pjf.log.Level", true );

        newFieldInApi.add( fieldInBothApi );
        newFieldInApi.add( onlyInNewApi );

        oldFieldInApi.add( fieldInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( null, newFieldInApi, null );
        PjfComponentApi oldApi = new PjfComponentApi( null, oldFieldInApi, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getFieldDiff().getAddedFields().size() );
        assertEquals( onlyInNewApi, apiComparator.getFieldDiff().getAddedFields().get( 0 ) );
    }

    @Test
    public void AddedFieldtest2()
    {
        ArrayList< Field > newFieldInApi = new ArrayList<>();
        ArrayList< Field > oldFieldInApi = new ArrayList<>();

        Field fieldOne = new Field( "de.psi.pjf.log.Level.class", "FATAL", "de.psi.pjf.log.Level", true );
        Field fieldTwo = new Field( "de.psi.pjf.log.Level.class", "DEBUG", "de.psi.pjf.log.Level", true );
        Field fieldThree = new Field( "de.psi.pjf.log.Level.class", "INFO", "de.psi.pjf.log.Level", true );

        newFieldInApi.add( fieldOne );
        newFieldInApi.add( fieldTwo );
        newFieldInApi.add( fieldThree );

        PjfComponentApi newApi = new PjfComponentApi( null, newFieldInApi, null );
        PjfComponentApi oldApi = new PjfComponentApi( null, oldFieldInApi, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getFieldDiff().getAddedFields().size() );
        assertEquals( fieldOne, apiComparator.getFieldDiff().getAddedFields().get( 0 ) );
        assertEquals( fieldTwo, apiComparator.getFieldDiff().getAddedFields().get( 1 ) );
        assertEquals( fieldThree, apiComparator.getFieldDiff().getAddedFields().get( 2 ) );
    }

    @Test
    public void RemovedFieldtest()
    {

        ArrayList< Field > newFieldInApi = new ArrayList<>();
        ArrayList< Field > oldFieldInApi = new ArrayList<>();

        Field fieldInBothApi =
            new Field( "de.psi.pjf.log.Level.class", "FATAL", "de.psi.pjf.log.Level", true );
        Field onlyInOldApi = new Field( "de.psi.pjf.log.Level.class", "DEBUG", "de.psi.pjf.log.Level", true );

        oldFieldInApi.add( fieldInBothApi );
        oldFieldInApi.add( onlyInOldApi );

        newFieldInApi.add( fieldInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( null, newFieldInApi, null );
        PjfComponentApi oldApi = new PjfComponentApi( null, oldFieldInApi, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getFieldDiff().getRemovedFields().size() );
        assertEquals( onlyInOldApi, apiComparator.getFieldDiff().getRemovedFields().get( 0 ) );
    }

    @Test
    public void RemovedFieldtest2()
    {
        ArrayList< Field > newFieldInApi = new ArrayList<>();
        ArrayList< Field > oldFieldInApi = new ArrayList<>();

        Field fieldOne = new Field( "de.psi.pjf.log.Level.class", "FATAL", "de.psi.pjf.log.Level", true );
        Field fieldTwo = new Field( "de.psi.pjf.log.Level.class", "DEBUG", "de.psi.pjf.log.Level", true );
        Field fieldThree = new Field( "de.psi.pjf.log.Level.class", "INFO", "de.psi.pjf.log.Level", true );

        oldFieldInApi.add( fieldOne );
        oldFieldInApi.add( fieldTwo );
        oldFieldInApi.add( fieldThree );

        PjfComponentApi newApi = new PjfComponentApi( null, newFieldInApi, null );
        PjfComponentApi oldApi = new PjfComponentApi( null, oldFieldInApi, null );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getFieldDiff().getRemovedFields().size() );
        assertEquals( fieldOne, apiComparator.getFieldDiff().getRemovedFields().get( 0 ) );
        assertEquals( fieldTwo, apiComparator.getFieldDiff().getRemovedFields().get( 1 ) );
        assertEquals( fieldThree, apiComparator.getFieldDiff().getRemovedFields().get( 2 ) );
    }

    @Test
    public void AddedConstructortest()
    {
        ArrayList< Constructor > newConstructorInApi = new ArrayList<>();
        ArrayList< Constructor > oldConstructorInApi = new ArrayList<>();

        Constructor constructorInBothApi =
            new Constructor( "de.psi.pjf.log.Log4JLoggerProvider.class", "none" );
        Constructor onlyInNewApi =
            new Constructor( "de.psi.pjf.log.LogConfigurationException.class", "none" );

        newConstructorInApi.add( constructorInBothApi );
        newConstructorInApi.add( onlyInNewApi );

        oldConstructorInApi.add( constructorInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( null, null, newConstructorInApi );
        PjfComponentApi oldApi = new PjfComponentApi( null, null, oldConstructorInApi );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getConstructorDiff().getAddedConstructors().size() );
        assertEquals( onlyInNewApi, apiComparator.getConstructorDiff().getAddedConstructors().get( 0 ) );
    }

    @Test
    public void AddedConstructortest2()
    {
        ArrayList< Constructor > newConstructorInApi = new ArrayList<>();
        ArrayList< Constructor > oldConstructorInApi = new ArrayList<>();

        Constructor constructorOne = new Constructor( "de.psi.pjf.log.Log4JLoggerProvider.class", "none" );
        Constructor constructorTwo =
            new Constructor( "de.psi.pjf.log.LogConfigurationException.class", "none" );
        Constructor constructorThree = new Constructor( "de.psi.pjf.log.AbstractLogger.class", "none" );

        newConstructorInApi.add( constructorOne );
        newConstructorInApi.add( constructorTwo );
        newConstructorInApi.add( constructorThree );

        PjfComponentApi newApi = new PjfComponentApi( null, null, newConstructorInApi );
        PjfComponentApi oldApi = new PjfComponentApi( null, null, oldConstructorInApi );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getConstructorDiff().getAddedConstructors().size() );
        assertEquals( constructorOne, apiComparator.getConstructorDiff().getAddedConstructors().get( 0 ) );
        assertEquals( constructorTwo, apiComparator.getConstructorDiff().getAddedConstructors().get( 1 ) );
        assertEquals( constructorThree, apiComparator.getConstructorDiff().getAddedConstructors().get( 2 ) );
    }

    @Test
    public void RemovedConstructortest()
    {
        ArrayList< Constructor > newConstructorInApi = new ArrayList<>();
        ArrayList< Constructor > oldConstructorInApi = new ArrayList<>();

        Constructor constructorInBothApi =
            new Constructor( "de.psi.pjf.log.Log4JLoggerProvider.class", "none" );
        Constructor onlyInNewApi =
            new Constructor( "de.psi.pjf.log.LogConfigurationException.class", "none" );

        oldConstructorInApi.add( constructorInBothApi );
        oldConstructorInApi.add( onlyInNewApi );

        newConstructorInApi.add( constructorInBothApi );

        PjfComponentApi newApi = new PjfComponentApi( null, null, newConstructorInApi );
        PjfComponentApi oldApi = new PjfComponentApi( null, null, oldConstructorInApi );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 1, apiComparator.getConstructorDiff().getRemovedConstructors().size() );
        assertEquals( onlyInNewApi, apiComparator.getConstructorDiff().getRemovedConstructors().get( 0 ) );
    }

    @Test
    public void RemovedConstructortest2()
    {
        ArrayList< Constructor > newConstructorInApi = new ArrayList<>();
        ArrayList< Constructor > oldConstructorInApi = new ArrayList<>();

        Constructor constructorOne = new Constructor( "de.psi.pjf.log.Log4JLoggerProvider.class", "none" );
        Constructor constructorTwo =
            new Constructor( "de.psi.pjf.log.LogConfigurationException.class", "none" );
        Constructor constructorThree = new Constructor( "de.psi.pjf.log.AbstractLogger.class", "none" );

        oldConstructorInApi.add( constructorOne );
        oldConstructorInApi.add( constructorTwo );
        oldConstructorInApi.add( constructorThree );

        PjfComponentApi newApi = new PjfComponentApi( null, null, newConstructorInApi );
        PjfComponentApi oldApi = new PjfComponentApi( null, null, oldConstructorInApi );

        ApiComparator apiComparator = new ApiComparator( oldApi, newApi );

        assertEquals( 3, apiComparator.getConstructorDiff().getRemovedConstructors().size() );
        assertEquals( constructorOne, apiComparator.getConstructorDiff().getRemovedConstructors().get( 0 ) );
        assertEquals( constructorTwo, apiComparator.getConstructorDiff().getRemovedConstructors().get( 1 ) );
        assertEquals( constructorThree,
            apiComparator.getConstructorDiff().getRemovedConstructors().get( 2 ) );
    }
}
