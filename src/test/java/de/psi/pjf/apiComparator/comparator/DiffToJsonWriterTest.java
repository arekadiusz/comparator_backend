package de.psi.pjf.apiComparator.comparator;

import static org.junit.Assert.fail;

import java.util.HashSet;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.psi.pjf.apiComparator.json.JsonInterpreterIf;
import de.psi.pjf.apiComparator.json.PackagesAndClassesParser;
import de.psi.pjf.apiComparator.processingdata.AbstractJavaApiElement;
import de.psi.pjf.apiComparator.processingdata.Constructor;
import de.psi.pjf.apiComparator.processingdata.Field;
import de.psi.pjf.apiComparator.processingdata.JavaClass;
import de.psi.pjf.apiComparator.processingdata.Method;

public class DiffToJsonWriterTest extends PackagesAndClassesParser
{
    @Test
    public void test()
    {
        HashSet< Method > hashSetMethod = new HashSet<>();
        hashSetMethod.add( new Method( "de.psi.pjf.dupa8.Class1.class", "met1", "String & Integer", "String",
            false, false ) );
        hashSetMethod
            .add( new Method( "de.psi.pjf.dupa8.Class1.class", "met1", "String", "String", false, false ) );
        hashSetMethod.add( new Method( "de.psi.pjf.dupa8.Class1.class", "met2", "String & Integer", "String",
            false, false ) );
        hashSetMethod.add( new Method( "de.psi.pjf.dupa8.Class2.class", "method", "String & Integer",
            "String", false, false ) );
        hashSetMethod.add( new Method( "de.psi.pjf.secondPackage.Class1.class", "method", "String & Integer",
            "String", false, false ) );

        HashSet< Field > hashSetField = new HashSet<>();
        hashSetField.add( new Field( "de.psi.pjf.dupa8.Class1.class", "fiel1", "String", false ) );
        hashSetField.add( new Field( "de.psi.pjf.dupa8.Class1.class", "fiel1", "Integer", false ) );
        hashSetField.add( new Field( "de.psi.pjf.dupa8.Class1.class", "fiel2", "String", true ) );
        hashSetField.add( new Field( "de.psi.pjf.dupa8.Class2.class", "field", "Integer", false ) );
        hashSetField.add( new Field( "de.psi.pjf.firstPackage.Class1.class", "field", "Integer", false ) );
        hashSetField.add( new Field( "de.psi.pjf.secondPackage.Class1.class", "field", "Integer", false ) );
        hashSetField.add( new Field( "de.psi.pjf.secondPackage2.Class1.class", "field", "Integer", false ) );

        HashSet< Constructor > hashSetConstructor = new HashSet<>();
        hashSetConstructor.add( new Constructor( "de.psi.pjf.dupa8.Class1.class", "String & Integer" ) );
        hashSetConstructor.add( new Constructor( "de.psi.pjf.dupa8.Class1.class", "String" ) );
        hashSetConstructor.add( new Constructor( "de.psi.pjf.dupa8.Class1.class", "Integer" ) );
        hashSetConstructor.add( new Constructor( "de.psi.pjf.dupa8.Class2.class", "String & Integer" ) );
        hashSetConstructor.add( new Constructor( "de.psi.pjf.secondPackage.Class1.class", "String" ) );

        JsonInterpreterIf addMethodIf = new JsonInterpreterIf()
        {
            @Override
            public void putJavaApiElementToCorrectList( JavaClass aClass, AbstractJavaApiElement aObject )
            {
                aClass.addAddedMethod( (Method)aObject );
                aClass.addRemovedMethod( (Method)aObject );
            }
        };
        JsonInterpreterIf addFieldIf = new JsonInterpreterIf()
        {
            @Override
            public void putJavaApiElementToCorrectList( JavaClass aClass, AbstractJavaApiElement aObject )
            {
                aClass.addAddedField( (Field)aObject );
                aClass.addRemovedField( (Field)aObject );
            }
        };
        JsonInterpreterIf addConstructorIf = new JsonInterpreterIf()
        {
            @Override
            public void putJavaApiElementToCorrectList( JavaClass aClass, AbstractJavaApiElement aObject )
            {
                aClass.addAddedConstructor( (Constructor)aObject );
                aClass.addRemovedConstructor( (Constructor)aObject );
            }
        };

        PackagesAndClassesParser packagesAndClassesParser = new PackagesAndClassesParser();

        packagesAndClassesParser.packagesAndClassesParse( hashSetMethod.stream(), addMethodIf );
        packagesAndClassesParser.packagesAndClassesParse( hashSetField.stream(), addFieldIf );
        packagesAndClassesParser.packagesAndClassesParse( hashSetConstructor.stream(), addConstructorIf );

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        gson.toJson( packagesAndClassesParser.getPackagesList() );

//        fail();
    }

}
